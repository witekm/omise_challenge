# frozen_string_literal: true

require 'helpers/factories/omise'

Payments::RetrieveToken.class_eval do
  def call
    Factories::Omise::TOKEN
  end
end

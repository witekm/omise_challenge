# frozen_string_literal: true

Payments::CreateCharge.class_eval do
  def call
    OpenStruct.new(amount: amount, paid: amount != 99_900)
  end
end

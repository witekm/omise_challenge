# frozen_string_literal: true

module Factories
  module Omise
    TOKEN = OpenStruct.new(
      id: 'tokn_X',
      card: OpenStruct.new(
        name: 'J DOE',
        last_digits: '4242',
        expiration_month: 10,
        expiration_year: 2020,
        security_code_check: false
      )
    )
  end
end

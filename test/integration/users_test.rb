# frozen_string_literal: true

require 'test_helper'

class UsersTest < ActionDispatch::IntegrationTest
  test 'admin can reach sign up page' do
    get new_user_registration_path

    assert_response :success
  end
end

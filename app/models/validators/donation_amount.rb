# frozen_string_literal: true

module Validators
  class DonationAmount < ActiveModel::Validator
    def validate(record)
      record.errors[:amount] << 'is too small' if record.amount.to_i < 21
    end
  end
end

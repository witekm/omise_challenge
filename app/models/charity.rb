# frozen_string_literal: true

class Charity < ActiveRecord::Base
  validates :name, presence: true

  def credit_amount(amount)
    Charity.transaction do
      lock!
      update_column :total, total + amount
    end
  end
end

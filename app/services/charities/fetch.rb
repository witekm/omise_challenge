# frozen_string_literal: true

module Charities
  class Fetch
    include Concerns::Callable

    attr_accessor :charity_id

    def initialize(charity_id:)
      @charity_id = charity_id
    end

    def call
      return random_charity if charity_id == 'random'

      fetch
    end

    private def fetch
      Charity.find_by(id: charity_id)
    end

    private def random_charity
      count = Charity.count
      random_offset = rand(count)
      Charity.offset(random_offset).first
    end
  end
end

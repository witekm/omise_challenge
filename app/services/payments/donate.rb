# frozen_string_literal: true

module Payments
  class Donate
    include Concerns::Callable

    attr_accessor :charity, :satangs_amount, :result, :token

    def initialize(charity:, satangs_amount:, token:)
      @charity = charity
      @result = false
      @satangs_amount = satangs_amount
      @token = token
    end

    def call
      return result unless correct_data?

      charge
      @result = charity.credit_amount(charge.amount) if charge.paid
      result
    end

    private def charge
      @charge ||= Payments::CreateCharge.call(charity: charity,
                                              satangs_amount: satangs_amount,
                                              token: token)
    end

    private def correct_data?
      charity.present? && satangs_amount.present? && token.present?
    end
  end
end

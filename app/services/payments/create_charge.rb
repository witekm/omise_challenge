# frozen_string_literal: true

module Payments
  class CreateCharge
    include Concerns::Callable

    attr_accessor :amount, :description, :token

    def initialize(charity:, satangs_amount:, token:)
      @amount = satangs_amount
      @description = "Donation to #{charity.name} [#{charity.id}]"
      @token = token
    end

    def call
      return nil unless amount.present? && token.present?

      Omise::Charge.create(
        amount: amount,
        currency: 'THB',
        card: token,
        description: description
      )
    end
  end
end

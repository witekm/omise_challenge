# frozen_string_literal: true

module Payments
  class RetrieveToken
    include Concerns::Callable

    attr_accessor :token

    def initialize(token:)
      @token = token
    end

    def call
      return nil unless token.present?

      Omise::Token.retrieve(token)
    end
  end
end

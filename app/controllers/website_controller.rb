# frozen_string_literal: true

class WebsiteController < ApplicationController
  include Concerns::Renderable

  before_action :omise_token_exists?, only: :donate

  def index
    @token = nil
  end

  def donate
    @form = DonationForm.new(donation_params)

    retrieve_token_and_render_failure and return unless @form.valid?

    success = Payments::Donate.call(charity: @form.charity_record,
                                    satangs_amount: @form.satangs_amount,
                                    token: params[:omise_token])

    success ? render_success(root_path) : render_failure(:index)
  end

  private def donation_params
    params.permit(:amount, :charity)
  end

  private def omise_token_exists?
    render_failure(:index) unless params[:omise_token].present?
  end

  private def retrieve_token_and_render_failure
    @token = Payments::RetrieveToken.call(token: params[:omise_token])
    render_failure(:index)
  end
end

# frozen_string_literal: true

class Admin::CharitiesController < ApplicationController
  include Concerns::Renderable

  before_action :authenticate_user!

  def index
    @charities = @app.all_charities
  end

  def new
    @charity = @app.build_charity
  end

  def create
    @charity = @app.create_charity(charity_params)

    if @charity.persisted?
      render_success(admin_charities_path)
    else
      render_failure(:new)
    end
  end

  private

  def charity_params
    params.require(:charity).permit(:name, :description)
  end
end

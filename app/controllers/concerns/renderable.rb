# frozen_string_literal: true

module Concerns
  module Renderable
    extend ActiveSupport::Concern

    def render_failure(destination, alert = t('.failure'))
      case destination
      when Symbol
        flash.now.alert = alert
        render destination
      when String
        flash.alert = alert
        redirect_to destination
      end
    end

    def render_success(destination, notice = t('.success'))
      case destination
      when Symbol
        flash.now.notice = notice
        render destination
      when String
        flash.notice = notice
        redirect_to destination
      end
    end
  end
end

# frozen_string_literal: true

class DonationForm
  include ActiveModel::Model

  attr_accessor :amount, :charity

  validates :amount, presence: true
  validates :charity, presence: true
  validates_with Validators::DonationAmount

  def satangs_amount
    amount.to_i * 100 + amount.split('.')[1].to_i
  end

  def charity_record
    Charities::Fetch.call(charity_id: charity)
  end
end

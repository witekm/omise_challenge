# frozen_string_literal: true

charities = YAML.load_file(Rails.root.join('test', 'fixtures', 'charities.yml').to_s)

app = App.new

charities.each do |_, c|
  app.create_charity(name: c['name']) unless Charity.exists?(name: c['name'])
end
